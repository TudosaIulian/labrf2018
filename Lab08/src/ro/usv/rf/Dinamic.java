package ro.usv.rf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
public  class Dinamic 
{
	public static int[] classifySet(double[][] x,int n, int p)
	{
		double[][] c = new double[x.length][x[0].length];
		int[] iClass = new int[x.length];
		
		boolean done = false;

		for(int i=0;i<x.length;i++)
		{
			c[i]= initiale(x[i]);
			
		}

		while(!done)
		{
			
			done = true;
			double[][] centre = new double[x.length][x[0].length];
			int[] pattern = new int [x.length];
			int kmin = 0;
			for(int i=0;i<x.length;i++)
			{
				double minDistance = Double.MAX_VALUE;
				for(int l=0;l<x[0].length;l++)
				{
					double distanceToKernel = calculateDistance(x[i],c[l]);
					if(distanceToKernel < minDistance)
					{
						minDistance= distanceToKernel;
						kmin=1;						
					}				
					
				}
				pattern[kmin]++;
				for(int j=0;j<x[0].length;j++)
				{
					centre[kmin][j] += x[i][j];
					
				}
				if(iClass[i]!=kmin)
				{
					iClass[i]=kmin;
					done =false;
					
				}
				
			}
			if(!done)
			{
				for(int i=0;i<x.length;i++)
				{
					for(int k=0;k<x[0].length;k++)
					{
						centre[i][k] /= pattern[k];
						
					}
					c[i] = updateKernel(centre[i],x);
				}
				
			}
		}
		return iClass;
		
	}	
	private static double[] initiale(double[] initialvalue)
	{
		double[] kernel = new double[initialvalue.length];
		for(int i=0;i<initialvalue.length;i++)
		{
			kernel[i] = initialvalue[i];
			
		}
		return kernel;
		
	}
	
	public static double calculateDistance(double[] vector1, double[] vector2) {
		double sum =0;	
		for(int i=0; i< vector1.length; i++){
			sum += Math.pow(vector1[i] - vector2[i], 2);
		}
		return Math.sqrt(sum);
	}
	private static double[] updateKernel(double[] centre, double[][] entrySet)
	{
		double min = Double.MAX_VALUE;
		int pattern =0;
	
		for(int i=0;i<entrySet.length;i++)
		{
			double distance = calculateDistance(entrySet[i],centre);
			if(distance <min)
			{
				min = distance;
				pattern =i;				
			}
			
		}		
		return entrySet[pattern];
	}
}
