package work;

import java.util.ArrayList;
import java.util.List;

public class MainClass {
	
	
	public static void main(String[] args) {
		double[][] learningSet;
		try {
			learningSet = FileUtils.readLearningSetFromFile("in.txt");
			int numberOfPatterns = learningSet.length; // nr de coloane
			int numberOfFeatures = learningSet[0].length; // nr de randuri
			System.out.println(String.format("The learning set has %s patterns/coloane and %s features/randuri", numberOfPatterns, numberOfFeatures));
	
			double[] firstPattern = learningSet[0];
			for(int i = 1; i<numberOfPatterns; i++) {
				System.out.println("Euclidian distance between 1 and " + (i+1) + " pattern: " + 
			euclidianDistance(firstPattern, learningSet[i]));
				System.out.println("Cebisev distance between 1 and " + (i+1) + " pattern: " + 
						cebisevDistance(firstPattern, learningSet[i]));
				System.out.println("City block distance between 1 and " + (i+1) + " pattern: " + 
						cityBlockDistance(firstPattern, learningSet[i]));
				System.out.println("Mahalanobis distance between 1 and " + (i+1) + " pattern: " + 
						mahalanobisDistance(firstPattern, learningSet[i],numberOfPatterns));
			}
			
//			System.out.println("lab4:" + EuclidianDistanceGEneralisedForm(learningSet,numberOfPatterns,numberOfFeatures));

		
		} catch (USVInputFileCustomException e) {
			System.out.println(e.getMessage());
		} finally {
			System.out.println("Finished learning set operations");
		}
	}
	
	public static double euclidianDistance(double[] pattern1, double[] pattern2)
	{
		return Math.sqrt((Math.pow(pattern1[0] - pattern2[0],  2)) + (Math.pow(pattern1[1] - pattern2[1],  2)));
	}
	
	public static double cebisevDistance(double[] pattern1, double[] pattern2)
	{
		double max = Math.abs(pattern1[0] - pattern2[0]);
		for(int i = 1 ; i<pattern1.length;i++) {
			double diff = pattern1[i] - pattern2[i];
			if(max < diff) max = diff;
		}
		
		return max;
	}
	
	public static double cityBlockDistance(double[] pattern1, double[] pattern2)
	{
		double sum = 0;
		for(int i = 0 ; i<pattern1.length;i++) {
			sum += Math.abs(pattern1[i] - pattern2[i]);
		}
		
		return sum;
	}
	
	public static double mahalanobisDistance(double[] pattern1, double[] pattern2, double n)
	{
		double sum = 0;
		for(int i = 0 ; i<pattern1.length;i++) {
			sum += Math.pow(pattern1[i] - pattern2[i], n);
		}
	
		return Math.pow(sum, 1/n);
	}
	
	public static double EuclidianDistanceGEneralisedForm(double[][] learningset,int numberOfPatterns,int numberOfFeatures)
	{
		
//		for(int i = 0; i< numberOfPatterns; i++)
//			for(int j= 0; j<=numberOfFeatures; j++)
//		{
//			
//			return  Math.sqrt( Math.abs( learningset[i][j] - learningset[i][j+1]));
//		}
		
	
		
	}
	
	
}
